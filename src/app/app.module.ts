import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardContainerComponent } from 'src/card-container/card-container.component';
import { CardComponent } from 'src/card-container/card/card.component';

@NgModule({
  declarations: [AppComponent, CardComponent, CardContainerComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
